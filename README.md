# Install Docker
sudo apt  install docker.io

# Clone Repository
cd /opt/docker/ <br/>
git clone https://gitlab.com/sinergidata/fastapi-docker.git

# Build image
docker build -t test-fastapi-image .

# Create and Run container
docker run -d --name test-fastapi-container -p 80:80 test-fastapi-image

# FastApi ready to use
