fastapi>=0.68.0,<0.69.0
pydantic>=1.8.0,<2.0.0
uvicorn>=0.15.0,<0.16.0
dnspython==1.16.0
typing-extensions==3.10.0.0
requests==2.20.0
OdooRPC==0.7.0
connection==0.0.0
python-dateutil==2.5.3
Jinja2==3.0.3
itsdangerous==2.0.1
werkzeug==0.16.1 

